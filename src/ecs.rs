use crate::{
    render_pipline::frame::{Pixel, Position},
    Object,
};

#[derive(Default)]
pub struct Ecs {
    pub(crate) inhalt: Vec<EcsNode>,
}

impl Ecs {
    pub fn add(&mut self, ecs_node: EcsNode) {
        self.inhalt.push(ecs_node);
    }
}

pub struct EcsNode {
    pub name: String,
    pub object: Box<dyn Object>,
    pub pixel_list: Vec<Pixel>,
    pub position: EPosition,
}

impl EcsNode {
    pub fn new(
        name: String,
        row: u16,
        column: u16,
        x: u16,
        y: u16,
        object: impl Object + 'static,
    ) -> Self {
        EcsNode {
            name,
            object: Box::new(object),
            pixel_list: vec![],
            position: EPosition {
                hight: row,
                width: column,
                x,
                y,
            },
        }
    }
}

impl std::ops::IndexMut<Position> for EcsNode {
    fn index_mut(&mut self, index: Position) -> &mut Self::Output {
        &mut self.pixel_list[(index.row * self.position.width + index.column) as usize]
    }
}

impl std::ops::Index<Position> for EcsNode {
    type Output = Pixel;

    fn index(&self, index: Position) -> &Self::Output {
        &self.pixel_list[(index.row * self.position.width + index.row) as usize]
    }
}

pub struct EPosition {
    pub width: u16,
    pub hight: u16,
    pub x: u16,
    pub y: u16,
}
