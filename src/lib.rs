use std::io::{self, stdout};

use crossterm::{cursor, terminal, ExecutableCommand};

pub mod ecs;
pub mod objects;
pub(crate) mod render_pipline;

use ecs::{Ecs, EcsNode};
use render_pipline::{frame::Pixel, frame_buffer::FrameBuffer};

pub trait Object {
    fn build(&self) -> Vec<Pixel>;
}

pub struct App {
    pub(crate) framebuffer: FrameBuffer,
    pub(crate) ecs: Ecs,
}

impl App {
    pub fn create_instance() -> io::Result<Self> {
        Ok(Self {
            framebuffer: FrameBuffer::new()?,
            ecs: Ecs::default(),
        })
    }

    pub fn add_object(&mut self, ecs_node: EcsNode) -> &mut Self {
        self.ecs.add(ecs_node);

        self
    }

    pub fn run(&mut self) -> io::Result<()> {
        let mut stdout = stdout();
        stdout.execute(terminal::Clear(terminal::ClearType::All))?;
        stdout.execute(cursor::Hide)?;
        loop {
            self.ecs
                .inhalt
                .iter()
                .for_each(|e| self.framebuffer.backendbuffer.add_objects(e));
            self.framebuffer.swap_buffer()?;
        }
    }
}
