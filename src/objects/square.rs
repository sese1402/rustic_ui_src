use crossterm::style::Color;

use crate::{render_pipline::frame::Pixel, Object};

pub struct Square {
    site_len: u16,
}

impl Square {
    pub fn new(site_len: u16) -> Self {
        Self { site_len }
    }
}

impl Object for Square {
    fn build(&self) -> Vec<Pixel> {
        (0..(self.site_len * self.site_len))
            .map(|_| Pixel::new(Color::Black, Color::White, '*'))
            .collect::<Vec<Pixel>>()
    }
}
