use std::io::{self, stdout};

use crossterm::{
    cursor::{self},
    queue,
    style::{self, Color, Stylize},
    terminal::{window_size, WindowSize},
};

use crate::ecs::EcsNode;

#[derive(Clone, Copy, Debug, PartialEq, PartialOrd)]
pub struct Pixel {
    pub(crate) bg_color: Color,
    pub(crate) fg_color: Color,
    pub(crate) char: char,
}

impl Pixel {
    pub fn new(bg_color: Color, fg_color: Color, char: char) -> Self {
        Self {
            bg_color,
            fg_color,
            char,
        }
    }
}

impl Default for Pixel {
    fn default() -> Self {
        Self {
            bg_color: Color::Black,
            fg_color: Color::White,
            char: ' ',
        }
    }
}

#[derive(Debug)]
pub(crate) struct Frame {
    pub(crate) pixel_list: Vec<Pixel>,
    pub(crate) window_size: WindowSize,
}

pub(crate) struct Position {
    pub row: u16,
    pub column: u16,
}

impl Position {
    pub fn new(row: u16, column: u16) -> Self {
        Self { row, column }
    }
}

impl std::ops::IndexMut<Position> for Frame {
    fn index_mut(&mut self, index: Position) -> &mut Self::Output {
        &mut self.pixel_list[(index.row * self.window_size.columns + index.column) as usize]
    }
}

impl std::ops::Index<Position> for Frame {
    type Output = Pixel;

    fn index(&self, index: Position) -> &Self::Output {
        &self.pixel_list[(index.row * self.window_size.columns + index.column) as usize]
    }
}

impl Frame {
    pub(crate) fn add_objects(&mut self, ecs_node: &EcsNode) {
        let min_row = ecs_node.position.y;
        let min_column = ecs_node.position.x;
        let max_row = ecs_node.position.width + ecs_node.position.y;
        let max_column = ecs_node.position.hight + ecs_node.position.x;

        for row in min_row..max_row {
            for column in min_column..max_column {
                self[Position::new(row, column)].char = '_';
            }
        }
    }

    pub(crate) fn new() -> io::Result<Self> {
        let mut frame = Self {
            pixel_list: vec![],
            window_size: window_size()?,
        };
        frame.fill_with_default_pixel();
        Ok(frame)
    }

    pub(crate) fn write_frame(&self) -> io::Result<()> {
        let mut stdout = stdout();

        for (i, pixel) in self.pixel_list.iter().enumerate() {
            let position = Position {
                row: (i / self.window_size.columns as usize) as u16,
                column: i as u16 % self.window_size.columns,
            };

            queue!(
                stdout,
                cursor::MoveTo(position.column, position.row),
                style::PrintStyledContent(pixel.char.on(pixel.bg_color).with(pixel.fg_color))
            )?;
        }

        Ok(())
    }

    pub(crate) fn refresh_pixel_list(&mut self) {
        self.pixel_list.clear();
        self.fill_with_default_pixel()
    }

    pub(crate) fn fill_with_default_pixel(&mut self) {
        let pixel_count = (self.window_size.columns * self.window_size.rows) as usize;
        let clear_pixel = vec![Pixel::default(); pixel_count];
        self.pixel_list.extend_from_slice(&clear_pixel);
    }
}

#[cfg(test)]
mod frame {
    use crossterm::{style::Color, terminal::WindowSize};

    use super::{Frame, Pixel, Position};

    fn list() -> Frame {
        Frame {
            pixel_list: vec![
                Pixel::new(Color::Red, Color::Red, '1'),
                Pixel::new(Color::Red, Color::Red, '2'),
                Pixel::new(Color::Red, Color::Red, '3'),
                Pixel::new(Color::Red, Color::Red, '4'),
                Pixel::new(Color::Red, Color::Red, '5'),
                Pixel::new(Color::Red, Color::Red, '6'),
                Pixel::new(Color::Red, Color::Red, '7'),
                Pixel::new(Color::Red, Color::Red, '8'),
                Pixel::new(Color::Red, Color::Red, '9'),
            ],
            window_size: WindowSize {
                rows: 3,
                columns: 3,
                width: 0,
                height: 0,
            },
        }
    }

    #[test]
    fn correct_position_2_2() {
        assert_eq!(
            list()[Position { column: 2, row: 2 }],
            Pixel::new(Color::Red, Color::Red, '9'),
        );
    }

    #[test]
    fn correct_position_1_2() {
        assert_eq!(
            list()[Position { column: 1, row: 2 }],
            Pixel::new(Color::Red, Color::Red, '8'),
        );
    }
}
