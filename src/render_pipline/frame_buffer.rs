use std::io;

use crossterm::terminal::window_size;

use super::frame::Frame;

pub struct FrameBuffer {
    pub(crate) frontendbuffer: Frame,
    pub(crate) backendbuffer: Frame,
}

impl FrameBuffer {
    pub(crate) fn new() -> io::Result<Self> {
        Ok(Self {
            frontendbuffer: Frame::new()?,
            backendbuffer: Frame::new()?,
        })
    }

    pub(crate) fn swap_buffer(&mut self) -> io::Result<()> {
        std::mem::swap(&mut self.backendbuffer, &mut self.frontendbuffer);
        self.frontendbuffer.write_frame()?;
        self.backendbuffer.window_size = window_size()?;
        self.backendbuffer.refresh_pixel_list();

        Ok(())
    }
}
